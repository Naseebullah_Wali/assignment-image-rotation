#include "../include/image.h"
#include "../include/bmp_header.h"
#include "../include/file_service.h"
#include "../include/status_enum.h"
#include "../include/transformation.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Invalid number of arguments \n");
        return 0;
    }

    FILE *input_file = {0};
    struct image first_image = {0};
    FILE *output_file = {0};
    
    const char* input_file_name = argv[1];
    const char* output_file_name = argv[2];

    enum status status = file_open(input_file_name, &input_file, "r");
    if (status != SUCCESS) {
        switch (status){
            case FILE_OPENING_ERROR: 
                // printf("Could not open file");
                fprintf(stderr,"Could not open file. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);
                // image_destroy(first_image);

            default: 
                // printf("Another ERROR");
                fprintf(stderr,"Another ERROR. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);
                // image_destroy(first_image);
        }
    }

    status = from_bmp(input_file, &first_image);
     if (status != SUCCESS) {
        switch (status){
            case READING_ERROR: 
                // printf("Error Reading File");
                fprintf(stderr,"Error Reading File. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);
                // image_destroy(first_image);
            default: 
                // printf("Another ERROR");
                fprintf(stderr,"Another ERROR.failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);
                // image_destroy(first_image);
        }
    }

    status = file_close(input_file);
    if (status != SUCCESS) {
        switch (status){
            case FILE_CLOSING_ERROR: 
                // printf("Failed to close file");      
                fprintf(stderr,"Failed to close file. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);      
                // image_destroy(first_image);
            default: 
                // printf("Another ERROR");   
                fprintf(stderr,"Another ERROR. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);             
                // image_destroy(first_image);
        }
    }

    struct image second_image = transform(&first_image);
    image_destroy(first_image);

    status = file_open(output_file_name, &output_file, "w");
    if (status != SUCCESS) {
        switch (status){
                case FILE_OPENING_ERROR: 
                // printf("Could not open file");
                fprintf(stderr,"Could not open file. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);
                // image_destroy(first_image);

            default: 
                // printf("Another ERROR");   
                fprintf(stderr,"Another ERROR. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);             
                // image_destroy(first_image);
        }
    }

    status = to_bmp(output_file, &second_image);
    if (status != SUCCESS) {
        switch (status){
            case WRITING_ERROR: 
                // printf("Error Writting to File");
                fprintf(stderr,"Error Writting to File. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);  
                // image_destroy(second_image);
                // image_destroy(first_image);
            default: 
                // printf("Another ERROR");   
                fprintf(stderr,"Another ERROR. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);             
                // image_destroy(first_image);
        }
    }

    status = file_close(output_file);
    if (status != SUCCESS) {
        switch (status){
            case FILE_CLOSING_ERROR: 
                // printf("Failed to close file");
                fprintf(stderr,"Failed to close file. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);
                // image_destroy(second_image);
                // image_destroy(first_image);
             default: 
                // printf("Another ERROR");   
                fprintf(stderr,"Another ERROR. failed in file %s at line # %d", __FILE__,__LINE__);
                exit(EXIT_FAILURE);             
                // image_destroy(first_image);
        }
    }
   
    image_destroy(second_image);
    printf("%s\n","SUCCESFULLY RESERVE");

    return 0;
}
