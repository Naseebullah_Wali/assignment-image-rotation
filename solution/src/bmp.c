#include "../include/bmp_header.h"
#include "stdint.h"
#include <stdbool.h>

static uint32_t size_of_padding(const uint32_t width) {
    if (width % 4 == 0) return 0;
    return 4 - ((width * 3) % 4);
}




static bool read_header(FILE* f, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

enum status from_bmp(FILE *in, struct image *image) {
    struct bmp_header header = {0};
    // if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
    //     return READING_ERROR;
    // }
    read_header(in, &header);

    if (header.biWidth <= 0 || header.biHeight <= 0 || header.bfileSize == 0 || header.biBitCount != 24) {
        return READING_ERROR;
    }
    
    uint32_t a = header.biWidth,b = header.biHeight;


    *image = image_create(a, b);

    uint32_t x = 0;
    uint32_t y = 0;
    
    while (x < image->height){
    	while (y < image->width){
    		fread(&(image->data[image->width * x + y]), sizeof(struct pixel), 1, in);
    		y++;
    	}
        
    	fseek(in, size_of_padding(image->width), SEEK_CUR);
    	x++;
    	y=0;
    }
    
    return SUCCESS;
}

enum status to_bmp(FILE *out, const struct image *image) {

    struct bmp_header header = {0};
   
    header.bfType = 19778;
    header.bfileSize = image->height * (3 * image->width + size_of_padding(image->width)) + sizeof(struct bmp_header);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = image->height * (3 * image->width + size_of_padding(image->width));
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
	
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITING_ERROR;
    }

    

    const uint8_t zero = 0;
    
    size_t x = 0;
    size_t y = 0;
    
    if (image->data) {
        while (x < image->height){
        	fwrite(image->data + x * image->width, image->width * sizeof(struct pixel), 1, out);
        	while (y < size_of_padding(image->width)){
        		fwrite(&zero, 1, 1, out);
        		y++;
        	}
       	x++;
        	y=0;
        }      
    } else {
        return WRITING_ERROR;
    }

    return SUCCESS;
}



